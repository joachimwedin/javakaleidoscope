The JavaKaleidoscope Compiler
------------------
This is a compiler implementation of the Kaleidoscope language from the [LLVM tutorials](https://llvm.org/docs/tutorial/MyFirstLanguageFrontend/index.html) written with Java and [JastAdd](https://jastadd.cs.lth.se/).
Unlike the tutorials, the compiler compiles source files (*.ks) with kaleidoscope code rather than using an interactive terminal.
To leverage LLVM from Java the compiler uses the [LLVM-C](https://llvm.org/doxygen/group__LLVMC.html) API through [Java JNA](https://github.com/java-native-access/jna).
The compiler supports compiling to object code, execution using a JIT engine and dumping LLVM IR.

Quickstart
------------------
The most reliable way to run the compiler is through docker.
```
git clone git@bitbucket.org:joachimwedin/javakaleidoscope.git
cd javakaleidoscope
docker build -t javakaleidoscope .
docker run -it javakaleidoscope 
java -jar KaleidoscopeCompiler.jar example.ks
clang++-11 example.o -o example
./example
```
The above commands will clone the source, set up a docker environment, compile and finally execute a Kaleidoscope program.
To see a full list of available options you can use `java -jar KaleidoscopeCompiler.jar -h`.

You can also run the compiler without docker, but it is less reliable.
To do this, make sure you have installed the same dependencies as is done in the docker file.
You can then build the compiler through `gradle jar` which will create the Kaleidoscope.jar file.

Example Kaleidoscope Programs
------------------
There are some [examples](https://bitbucket.org/joachimwedin/javakaleidoscope/src/master/exampleprogs) of Kaleidoscope programs available in the repository.
If you are interested, you can have a look at the [CFG](https://bitbucket.org/joachimwedin/javakaleidoscope/src/master/front-end/src/main/jastadd/KaleidoscopeParser.parser) (context-free grammar) for a full specification of the language.

