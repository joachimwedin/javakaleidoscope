package org.javakaleidoscope.test;

import org.javakaleidoscope.compiler.KaleidoscopeCompiler;
import org.junit.Test;

public class JavaKaleidoscopeTests extends ReferenceFileTest {

    private final KaleidoscopeCompiler compiler = new KaleidoscopeCompiler();
    private final ThrowingFunction<String, String> evalWithJit = compiler::executeWithJITEngine;

    @Override
    protected String myTestFileDirectory() {
        return "src/test/resources/testfiles/";
    }

    public void testEvalJIT(String testName) {
        performReferenceFileTest(testName, evalWithJit);
    }

    @Test
    public void numericalExpression() {
        testEvalJIT("numericalExpression");
    }

    @Test
    public void addExpression() {
        testEvalJIT("addExpression");
    }

    @Test
    public void functionDeclaration() {
        testEvalJIT("functionDeclaration");
    }

    @Test
    public void multipleTopLevelExpressions() {
        testEvalJIT("multipleTopLevelExpressions");
    }
}
