package org.javakaleidoscope.compiler;

import beaver.Parser;

import org.apache.commons.cli.*;

import java.io.*;

/**
 * The main class of the JavaKaleidoscope compiler.
 * The class exposes various methods for compiling kaleidoscope source files to different targets, see {@link CompilationTarget}.
 * It also defines a command line interface which makes it possible to compile to different compilation targets either from the command line or with the main method.
 */
public class KaleidoscopeCompiler {

    /**
     * An enumeration used for representing different compilation target.
     */
    enum CompilationTarget {
        OBJECT, // For generating object files.
        JIT, // For executing a JavaKaleidoscope IR using a JIT engine.
        IR // For dumping JavaKaleidoscope IR to a string.
    }

    private final KaleidoscopeFrontEnd frontend;
    private final KaleidoscopeMiddleEnd middleEnd;
    private final KaleidoscopeBackEnd backEnd;

    public KaleidoscopeCompiler() {
        frontend = new KaleidoscopeFrontEnd();
        middleEnd = new KaleidoscopeMiddleEnd();
        backEnd = new KaleidoscopeBackEnd();
    }

    /**
     * Compiles a kaleidoscope source file to a compilation target.
     *
     * @param target the target of the compilation.
     * @param sourceFile the path to the kaleidoscope source file, any kind of file ending is allowed.
     * @throws IOException if the source file cannot be read.
     * @throws Parser.Exception if the source code cannot be parsed.
     */
    public void compile(CompilationTarget target, String sourceFile) throws IOException, Parser.Exception {
        switch(target) {
            case OBJECT:
                generateObjectCode(sourceFile);
                return;
            case JIT:
                System.out.println(executeWithJITEngine(sourceFile));
                return;
            case IR:
                System.out.println(dumpIR(sourceFile));
                return;
            default:
                throw new IllegalArgumentException("Unknown compilation target: " + target);
        }
    }

    /**
     * The entry point for running the compiler. If no arguments are given then a help message is displayed.
     * To run the compiler you need to supply a single kaleidoscope source file and zero or more options.
     * See {@link KaleidoscopeCompiler#createOptions()} for valid options.
     *
     * @param args the commandline arguments to the compiler.
     * @throws ParseException  if the commandline arguments cannot be parsed.
     * @throws IOException if the source file cannot be read.
     * @throws Parser.Exception if the JavaKaleidoscope parser fails to parse the source code.
     */
    public static void main(String[] args) throws ParseException, IOException, Parser.Exception {
        Options options = createOptions();
        CommandLine cmd = parseCommandLineOptions(options, args);

        if (cmd.hasOption("help") || args.length == 0) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp( "java -jar KaleidoscopeCompiler.jar [OPTION]... [FILE]\nExample: java -jar KaleidoscopeCompiler.jar -t=ir foo.ks", options);
            return;
        }

        java.util.List<String> fileList = cmd.getArgList();
        if (fileList.size() != 1) {
            throw new IllegalArgumentException("A source file has to be provided.. See -h for more information.");
        }
        String file = fileList.get(0);

        CompilationTarget target = getCompilationTarget(cmd);
        KaleidoscopeCompiler compiler = new KaleidoscopeCompiler();
        compiler.compile(target, file);
    }

    /**
     * Gets the compilation target (see {@link CompilationTarget}) from a list of commandline arguments.
     *
     * @param cmd the commandline arguments.
     * @return the compilation target.
     */
    private static CompilationTarget getCompilationTarget(CommandLine cmd) {
        if (cmd.hasOption("target")) {
            String targetString = cmd.getOptionValue("target");
            try {
                return CompilationTarget.valueOf(targetString.toUpperCase().trim());
            } catch (IllegalArgumentException e) {
                throw new IllegalArgumentException("Unsupported target value provided. See -h for supported values.");
            }
        } else {
            return CompilationTarget.OBJECT;
        }
    }

    /**
     * Creates a parser and parses a list of commandline arguments.
     *
     * @param options an object describing the options for the JavaKaleidoscopeCompiler commandline interface.
     * @param args the list of arguments to parse.
     * @return the result of the parsing.
     * @throws ParseException if the list of arguments cannot be parsed.
     */
    private static CommandLine parseCommandLineOptions(Options options, String[] args) throws ParseException {
        CommandLineParser parser = new DefaultParser();
        return parser.parse(options, args);
    }

    /**
     * Defines the options of the compiler commandline interface.
     *
     * @return an object representing the options of the compiler.
     */
    private static Options createOptions() {
        Options options = new Options();
        Option targetOption = Option.builder("t")
                .longOpt("target")
                .desc("The compilation target. If no target is provided the value defaults to object. Supported values:\n"
                        + "object - Builds an object file\n"
                        + "jit - Executes using a JIT engine\n"
                        + "ir - Dumps LLVM IR")
                .hasArg()
                .argName("TARGET")
                .build();
        Option helpOption = Option.builder("h")
                .longOpt("help")
                .desc("Print this message")
                .build();
        options.addOption(targetOption);
        options.addOption(helpOption);
        return options;
    }

    /**
     * Executes a kaleidoscope source file using a JIT engine.
     *
     * @param sourceFile the path to the kaleidoscope source file to execute.
     * @return the result of evaluating all top level expressions and joining the result with newlines to a string.
     * @throws IOException if the source file cannot be read.
     * @throws Parser.Exception if the source file cannot be parsed.
     */
    public String executeWithJITEngine(String sourceFile) throws IOException, Parser.Exception {
        KaleidoscopeIR ir = generateTransformedKaleidoscopeIR(sourceFile);
        return backEnd.executeWithJITEngine(ir);
    }

    /**
     * Builds an IR by passing a kaleidoscope source file through the front and middle end of the compiler.
     *
     * @param sourceFile the path to the source file to build IR for.
     * @return the generated IR.
     * @throws IOException if the source file cannot be read.
     * @throws Parser.Exception if the source file cannot be parsed.
     */
    public KaleidoscopeIR generateTransformedKaleidoscopeIR(String sourceFile) throws IOException, Parser.Exception {
        KaleidoscopeIR ir = frontend.generateKaleidoscopeIR(sourceFile);
        middleEnd.transform(ir);
        return ir;
    }

    /**
     * Passes a kaleidoscope source file through the front and middle end of the compiler and dumps the resulting IR to a string.
     *
     * @param sourceFile the path to the source file to build IR for.
     * @return the string representation of the IR.
     * @throws IOException if the source file cannot be read.
     * @throws Parser.Exception if the source file cannot be parsed.
     */
    public String dumpIR(String sourceFile) throws IOException, Parser.Exception {
        return generateTransformedKaleidoscopeIR(sourceFile).dumpIR();
    }

    /**
     * Passes a kaleidoscope source file through the front and middle end of the compiler and builds an object file from the LLVM IR.
     *
     * @param sourceFile the path to the kaleidoscope source file.
     * @throws IOException if the source file cannot be read.
     * @throws Parser.Exception if the source file cannot be parsed.
     */
    public void generateObjectCode(String sourceFile) throws IOException, Parser.Exception {
        KaleidoscopeIR ir = generateTransformedKaleidoscopeIR(sourceFile);
        backEnd.genObjectCode(ir);
    }
}
