package org.javakaleidoscope.test;

import org.javakaleidoscope.compiler.KaleidoscopeFrontEnd;
import org.junit.Test;

public class KaleidoscopeFrontendTests extends ReferenceFileTest {

    private final KaleidoscopeFrontEnd compiler = new KaleidoscopeFrontEnd();
    private final ThrowingFunction<String, String> generateIR = compiler::dumpIR;

    @Override
    protected String myTestFileDirectory() {
        return "src/test/resources/testfiles/";
    }

    public void testGeneratedIR(String fileName) {
        performReferenceFileTest(fileName, generateIR);
    }

    @Test
    public void singleFunctionDefinition() {
        testGeneratedIR("singleFunctionDefinition");
    }

    @Test
    public void multipleFunctionDefinitions() {
        testGeneratedIR("multipleFunctionDefinitions");
    }

    @Test
    public void topLevelNumericalExpression() {
        testGeneratedIR("topLevelNumericalExpression");
    }

    @Test
    public void multipleTopLevelExpressions() {
        testGeneratedIR("multipleTopLevelExpressions");
    }

    @Test
    public void singleFunctionCall() {
        testGeneratedIR("singleFunctionCall");
    }

    @Test
    public void singleFunctionCallNoArgs() {
        testGeneratedIR("singleFunctionCallNoArgs");
    }

    @Test
    public void nestedFunctionCalls() {
        testGeneratedIR("nestedFunctionCalls");
    }
}
