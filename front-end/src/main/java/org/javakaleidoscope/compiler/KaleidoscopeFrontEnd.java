package org.javakaleidoscope.compiler;

import AST.KaleidoscopeParser;
import AST.KaleidoscopeScanner;
import AST.ProgramAST;
import beaver.Parser;
import com.sun.jna.Native;
import org.apache.commons.io.FilenameUtils;
import org.jllvm.JLlvm;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

public class KaleidoscopeFrontEnd {

    public static void main(String[] args) throws IOException, Parser.Exception {
        if (args.length != 1) {
            System.out.println("Incorrect number of parameters. Example usage: java -jar kaleidoscope-frontend.jar foo.ks");
        }
        KaleidoscopeFrontEnd frontend = new KaleidoscopeFrontEnd();
        System.out.println(frontend.dumpIR(args[0]));
    }

    static JLlvm LLVM;
    static {
        loadLLVMLibrary();
    }

    /**
     * Extracts the libmyLlvmLibrary.so file from the resource path to a temporary directory and loads its.
     * This is done because the file cannot be loaded if it is located inside a jar file.
     */
    private static void loadLLVMLibrary() {
        try {
            String myLibraryName = "libmyLlvmLibrary.so";
            File myTemporaryLibraryDirectory = Files.createTempDirectory("myLlvmLibrary").toFile();
            myTemporaryLibraryDirectory.deleteOnExit();
            File myTemporaryLibraryFile = new File(myTemporaryLibraryDirectory, myLibraryName);
            myTemporaryLibraryFile.deleteOnExit();
            InputStream in = JLlvm.class.getResource("/" + myLibraryName).openStream();
            Files.copy(in, myTemporaryLibraryFile.toPath());
            System.load(myTemporaryLibraryFile.getAbsolutePath());
            LLVM = Native.loadLibrary(myTemporaryLibraryFile.getAbsolutePath(), JLlvm.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Generates LLVM IR for a source file and dumps it to a string.
     *
     * @param file the path to the source file
     * @return the generated IR as a string
     * @throws IOException
     * @throws Parser.Exception
     */
    public String dumpIR(String file) throws IOException, Parser.Exception {
        return generateKaleidoscopeIR(file).dumpIR();
    }

    /**
     * Generates Kaleidoscope IR for a source file.
     *
     * @param file the path to the source file
     * @return the generated Kaleidoscope IR
     * @throws IOException
     * @throws Parser.Exception
     */
    public KaleidoscopeIR generateKaleidoscopeIR(String file) throws IOException, Parser.Exception {
        ProgramAST ast = parseFile(file);
        return ast.genIR(FilenameUtils.removeExtension(file));
    }

    /**
     * Builds the AST for a source file.
     *
     * @param file the path to the source file
     * @return the generated AST
     * @throws IOException
     * @throws Parser.Exception
     */
    public ProgramAST parseFile(String file) throws IOException, Parser.Exception {
        FileReader reader = new FileReader(file);
        KaleidoscopeScanner scanner = new KaleidoscopeScanner(reader);
        KaleidoscopeParser parser = new KaleidoscopeParser();
        return (ProgramAST) parser.parse(scanner);
    }
}
