package AST; // The generated parser will belong to package AST

import AST.KaleidoscopeParser.Terminals; // The terminals are implicitly defined in the parser

%%

// define the signature for the generated scanner
%public
%final
%class KaleidoscopeScanner
%extends beaver.Scanner

// the interface between the scanner and the parser is the nextToken() method
%type beaver.Symbol 
%function nextToken 
%yylexthrow beaver.Scanner.Exception

// store line and column information in the tokens
%line
%column

// this code will be inlined in the body of the generated scanner class
%{
  private beaver.Symbol sym(short id) {
    return new beaver.Symbol(id, yyline + 1, yycolumn + 1, yylength(), yytext());
  }
%}

WhiteSpace = [ ] | \t | \f | \n | \r | \r\n
Identifier = [a-zA-Z]+[a-zA-Z0-9]*
Number = [0-9.]+

%%

// discard whitespace information
{WhiteSpace}  { }

// add support for extern
// reconsider ordering of these
// token definitions
// "extern"      { return sym(Terminals.EXTERN); }
"def"      { return sym(Terminals.DEFINITION); }
{Identifier}  { return sym(Terminals.IDENTIFIER); }
";"      { return sym(Terminals.SEMICOLON); }
"("      { return sym(Terminals.LEFT_PARENTHESIS); }
")"      { return sym(Terminals.RIGHT_PARENTHESIS); }
"+"      { return sym(Terminals.PLUS); }
"*"      { return sym(Terminals.ASTERISK); }
"-"      { return sym(Terminals.HYPHEN); }
","      { return sym(Terminals.COMMA); }
{Number}      { return sym(Terminals.NUMBER); }
<<EOF>>       { return sym(Terminals.EOF); }
[^] { throw new UnsupportedOperationException("Illegal character <"+yytext()+">"); }
