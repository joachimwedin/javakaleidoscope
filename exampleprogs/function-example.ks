def bar(x)
    x + 1;

def foo(x, y, z)
    x + y + bar(z);

foo(1, 2, 3);