package org.javakaleidoscope.compiler;

import org.jllvm.pointers.LLVMModuleRef;
import org.jllvm.pointers.LLVMPassManagerRef;
import org.jllvm.pointers.LLVMValueRef;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.javakaleidoscope.compiler.KaleidoscopeIR.LLVM;

public class KaleidoscopeMiddleEnd {
    /**
     * Transforms an LLVM IR by applying transformations to it.
     * @param ir the IR to transform.
     */
    public void transform(KaleidoscopeIR ir) {
        createAndRunFunctionPassManager(ir);
    }

    /**
     * Utility method that creates a function pass manager using {@link KaleidoscopeMiddleEnd#createFunctionPassManager(LLVMModuleRef)}
     * and then runs it on all functions of an IR.
     *
     * @param ir the IR to run function passes on.
     */
    private void createAndRunFunctionPassManager(KaleidoscopeIR ir) {
        for (LLVMModuleRef module: ir.getModules()) {
            LLVMPassManagerRef functionPassManager = createFunctionPassManager(module);
            for (LLVMValueRef function: getModuleFunctions(module)) {
                String name = LLVM.LLVMGetValueName2(function, new int[] {0});
                if (!name.equals("printf") && !name.equals("main")) {
                    LLVM.LLVMRunFunctionPassManager(functionPassManager, function);
                }
            }
        }
    }

    /**
     * Collects all the functions of an LLVM module.
     * @param module the module to collect functions for
     * @return the result collection of functions
     */
    private Collection<LLVMValueRef> getModuleFunctions(LLVMModuleRef module) {
        List<LLVMValueRef> functions = new ArrayList<>();
        LLVMValueRef func = LLVM.LLVMGetFirstFunction(module);
        while (func != null) {
            functions.add(func);
            func = LLVM.LLVMGetNextFunction(func);
        }
        return functions;
    }

    /**
     * Creates a LLVM function pass manager that applies the following passes:
     * - Instruction Combining
     * - Reassociate
     * - GVN
     * - CFG Simplification
     *
     * @param module the module used to created the function pass manager.
     * @return the created function pass managera.
     */
    private LLVMPassManagerRef createFunctionPassManager(LLVMModuleRef module) {
        LLVMPassManagerRef passManager = LLVM.LLVMCreateFunctionPassManagerForModule(module);
        LLVM.LLVMAddInstructionCombiningPass(passManager);
        LLVM.LLVMAddReassociatePass(passManager);
        LLVM.LLVMAddGVNPass(passManager);
        LLVM.LLVMAddCFGSimplificationPass(passManager);
        LLVM.LLVMInitializeFunctionPassManager(passManager);
        return passManager;
    }
}
