package org.javakaleidoscope.compiler;

import com.sun.jna.Native;
import org.jllvm.JLlvm;
import org.jllvm.pointers.LLVMBuilderRef;
import org.jllvm.pointers.LLVMContextRef;
import org.jllvm.pointers.LLVMModuleRef;

import java.util.Collection;
import java.util.stream.Collectors;

public class KaleidoscopeIR {

    private final LLVMContextRef context;
    private final Collection<LLVMModuleRef> modules;
    private final LLVMBuilderRef builder;
    public static JLlvm LLVM = JLlvm.INSTANCE;

    /**
     * The JavaKaleidoscope IR (intermediate representation).
     * This is essentially a Java wrapper class to the corresponding c++ LLVM IR.
     * @param context the LLVM context of the IR.
     * @param modules the modules of the IR
     * @param builder the LLVM builder of the IR.
     */
    public KaleidoscopeIR(LLVMContextRef context, Collection<LLVMModuleRef> modules, LLVMBuilderRef builder) {
        this.context = context;
        this.modules = modules;
        this.builder = builder;
    }

    public Collection<LLVMModuleRef> getModules() {
        return modules;
    }
    public LLVMContextRef getContext() {
        return context;
    }
    public LLVMBuilderRef getBuilder() {
        return builder;
    }

    /**
     * Dumps the string representation of this IR by printing each module to string and joining them with a newline.
     * @return the string representation of the IR.
     */
    public String dumpIR() {
        return modules.stream()
                .map(module -> LLVM.LLVMPrintModuleToString(module))
                .collect(Collectors.joining("\n"));
    }
}
