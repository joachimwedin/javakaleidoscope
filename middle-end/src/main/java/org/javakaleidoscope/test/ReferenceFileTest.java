package org.javakaleidoscope.test;

import org.junit.Assert;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.Assert.fail;
public abstract class ReferenceFileTest {

    /**
     * A functional interface for representing a non void function with a single parameter that can throw an exception.
     * @param <T>
     * @param <R>
     */
    public interface ThrowingFunction<T, R> {
        R apply(T t) throws Exception;
    }

    /**
     * A utility method for testing the return value of a function against a reference file.
     * The location where the reference file is searched for is determined by {@link ReferenceFileTest#myTestFileDirectory()} and the first parameter.
     * For example, if you want the test to look for the reference in /foo/bar/baz.input then myTestFileDirectory should return /foo/bar/ and the first parameter should be baz (the .input ending is added automatically).
     * @param testName the name of the file with the reference result (without the .input file ending).
     * @param testMethod the method to test
     */
    public void performReferenceFileTest(String testName, ThrowingFunction<String, String> testMethod) {
        String testFileDirectory = myTestFileDirectory();
        String inputFile = testFileDirectory + testName + ".input";
        String referenceFile = testFileDirectory + testName + ".expected";

        String actual = null;
        try {
            actual = testMethod.apply(inputFile);
        } catch (Exception e) {
            fail("Received unexpected exception");
        }
        String expected;
        try {
            byte[] bytes = Files.readAllBytes(Paths.get(referenceFile));
            expected = new String(bytes, StandardCharsets.UTF_8);
        } catch (IOException e) {
            fail("Failed to read reference file.");
            return;
        };

        Assert.assertEquals(expected, actual);
    }

    /**
     * Returns the path to the directory with reference files.
     */
    protected abstract String myTestFileDirectory();
}

