FROM ubuntu:18.04

# LLVM
RUN apt-get update \
    && apt-get install lsb-release wget software-properties-common -y \
    && wget https://apt.llvm.org/llvm.sh \
    && chmod +x llvm.sh \
    && ./llvm.sh 11

# JavaKaleidoscope
RUN apt-get install git gradle make -y
RUN git clone https://joachimwedin@bitbucket.org/joachimwedin/javakaleidoscope.git
RUN cd javakaleidoscope && gradle jar

WORKDIR /javakaleidoscope/
