package org.jllvm;

import org.jllvm.pointers.LLVMExecutionEngineRef;
import org.jllvm.pointers.LLVMModuleRef;
import org.jllvm.pointers.LLVMTargetMachineRef;

public interface CustomJLlvmMethods {
    void initializeNative();
    void initializeAll();

    LLVMTargetMachineRef createDefaultTargetMachine();
    void emitObjectFile(LLVMTargetMachineRef targetMachine, LLVMModuleRef module, String fileName);

    LLVMExecutionEngineRef createExecutionEngine(LLVMModuleRef mod);
    int runIntProducerFunction(LLVMExecutionEngineRef engine, String functionName);
    String runStringProducerFunction(LLVMExecutionEngineRef engine, String functionName);
}
