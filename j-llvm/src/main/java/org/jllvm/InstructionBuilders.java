package org.jllvm;

import org.jllvm.pointers.LLVMBasicBlockRef;
import org.jllvm.pointers.LLVMBuilderRef;
import org.jllvm.pointers.LLVMTypeRef;
import org.jllvm.pointers.LLVMValueRef;

public interface InstructionBuilders {
    LLVMBuilderRef LLVMCreateBuilder();
    void LLVMPositionBuilderAtEnd(LLVMBuilderRef builder, LLVMBasicBlockRef block);
    LLVMValueRef LLVMBuildAdd(LLVMBuilderRef builder, LLVMValueRef lhs, LLVMValueRef rhs, String name);
    LLVMValueRef LLVMBuildFAdd(LLVMBuilderRef builder, LLVMValueRef lhs, LLVMValueRef rhs, String name);
    LLVMValueRef LLVMBuildFSub(LLVMBuilderRef builder, LLVMValueRef lhs, LLVMValueRef rhs, String name);
    LLVMValueRef LLVMBuildFMul(LLVMBuilderRef builder, LLVMValueRef lhs, LLVMValueRef rhs, String name);
    LLVMValueRef LLVMBuildRet(LLVMBuilderRef builder, LLVMValueRef returnValue);
    LLVMValueRef LLVMBuildCall(LLVMBuilderRef builder, LLVMValueRef function, LLVMValueRef[] args, int numArgs, String name);
    LLVMValueRef LLVMBuildAlloca(LLVMBuilderRef builder, LLVMTypeRef Ty, String name);
    LLVMValueRef LLVMBuildArrayAlloca(LLVMBuilderRef builder, LLVMTypeRef type, LLVMValueRef val, String name);
    LLVMValueRef LLVMBuildArrayMalloc(LLVMBuilderRef builder, LLVMTypeRef type, LLVMValueRef val, String name);
    LLVMValueRef LLVMBuildStore(LLVMBuilderRef builder, LLVMValueRef val, LLVMValueRef ptr);
    LLVMValueRef LLVMBuildMemCpy(LLVMBuilderRef builder, LLVMValueRef destination, int destinationAlignment,
                                 LLVMValueRef source, int sourceAlignment, LLVMValueRef size);
    LLVMValueRef LLVMBuildGEP(LLVMBuilderRef builder, LLVMValueRef pointer, LLVMValueRef[] indices, int	numIndices, String name);
    LLVMValueRef LLVMBuildGEP2(LLVMBuilderRef builder, LLVMTypeRef type, LLVMValueRef pointer, LLVMValueRef[] indices, int	numIndices, String name);
    LLVMValueRef LLVMBuildGlobalString(LLVMBuilderRef builder, String str, String name);
}
