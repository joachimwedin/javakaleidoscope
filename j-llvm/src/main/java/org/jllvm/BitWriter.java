package org.jllvm;

import org.jllvm.pointers.LLVMModuleRef;

public interface BitWriter {
    int LLVMWriteBitcodeToFile(LLVMModuleRef module, String path);
}
