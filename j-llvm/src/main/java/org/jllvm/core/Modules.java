package org.jllvm.core;

import org.jllvm.pointers.LLVMModuleRef;
import org.jllvm.pointers.LLVMTypeRef;
import org.jllvm.pointers.LLVMValueRef;

public interface Modules {
    LLVMModuleRef LLVMModuleCreateWithName(String name);
    void LLVMSetDataLayout(LLVMModuleRef m, String dataLayoutStr);
    LLVMValueRef LLVMAddFunction(LLVMModuleRef module, String name, LLVMTypeRef functionType);
    LLVMValueRef LLVMGetNamedFunction(LLVMModuleRef module, String name);
    void LLVMSetTarget(LLVMModuleRef module, String triple);
    void LLVMPrintModuleToFile(LLVMModuleRef myModule, String s, String error);
    String LLVMPrintModuleToString(LLVMModuleRef myModule);
    LLVMValueRef LLVMGetFirstFunction(LLVMModuleRef module);
    LLVMValueRef LLVMGetNextFunction(LLVMValueRef func);
    LLVMValueRef LLVMGetLastFunction(LLVMModuleRef module);
    String LLVMGetModuleIdentifier (LLVMModuleRef module, int[] len);
}
