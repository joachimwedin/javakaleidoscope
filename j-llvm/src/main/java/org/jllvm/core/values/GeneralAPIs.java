package org.jllvm.core.values;

import org.jllvm.pointers.LLVMValueRef;

public interface GeneralAPIs {
    void LLVMSetValueName(LLVMValueRef value, String name);
    String LLVMGetValueName2(LLVMValueRef value, int[] length);
}
