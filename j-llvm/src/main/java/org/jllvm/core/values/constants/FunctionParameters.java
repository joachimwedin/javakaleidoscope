package org.jllvm.core.values.constants;

import org.jllvm.pointers.LLVMValueRef;

public interface FunctionParameters {
    LLVMValueRef LLVMGetParam(LLVMValueRef function, int index);
}
