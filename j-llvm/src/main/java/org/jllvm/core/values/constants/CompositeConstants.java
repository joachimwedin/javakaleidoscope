package org.jllvm.core.values.constants;

import org.jllvm.pointers.LLVMContextRef;
import org.jllvm.pointers.LLVMValueRef;

public interface CompositeConstants {
    LLVMValueRef LLVMConstStringInContext(LLVMContextRef C, String str, int length, int dontNullTerminate);
}
