package org.jllvm.core.values.constants;

import org.jllvm.pointers.LLVMTypeRef;
import org.jllvm.pointers.LLVMValueRef;

public interface ScalarConstants {
    LLVMValueRef LLVMConstReal(LLVMTypeRef realType, double value);
    LLVMValueRef LLVMConstInt(LLVMTypeRef intType, long N, int signExtend);
}
