package org.jllvm.core.types;

import org.jllvm.pointers.LLVMTypeRef;

public interface SequentialTypes {
    LLVMTypeRef LLVMPointerType(LLVMTypeRef elementType, int addressSpace);
}
