package org.jllvm.core.types;

import org.jllvm.pointers.LLVMTypeRef;

public interface FunctionTypes {
    LLVMTypeRef LLVMFunctionType(LLVMTypeRef returnType, LLVMTypeRef[] paramTypes, int paramCount, int isVarArg);
}
