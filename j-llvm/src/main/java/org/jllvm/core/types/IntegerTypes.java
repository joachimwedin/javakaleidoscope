package org.jllvm.core.types;

import org.jllvm.pointers.LLVMTypeRef;

public interface IntegerTypes {
    LLVMTypeRef LLVMInt8Type();
    LLVMTypeRef LLVMInt32Type();
    LLVMTypeRef LLVMInt64Type();
    LLVMTypeRef LLVMDoubleType();
}
