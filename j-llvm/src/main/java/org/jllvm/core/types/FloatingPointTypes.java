package org.jllvm.core.types;

import org.jllvm.pointers.LLVMTypeRef;

public interface FloatingPointTypes {
    LLVMTypeRef LLVMFloatType();
}
