package org.jllvm.core;

import org.jllvm.pointers.LLVMContextRef;

public interface Contexts {
    LLVMContextRef LLVMContextCreate();
}
