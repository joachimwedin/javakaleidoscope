package org.jllvm.core;

import org.jllvm.pointers.LLVMBasicBlockRef;
import org.jllvm.pointers.LLVMValueRef;

public interface BasicBlock {
    LLVMBasicBlockRef LLVMAppendBasicBlock(LLVMValueRef function, String name);
}
