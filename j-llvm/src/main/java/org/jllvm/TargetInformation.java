package org.jllvm;

import org.jllvm.pointers.LLVMTargetDataRef;

public interface TargetInformation {
    String LLVMCopyStringRepOfTargetData(LLVMTargetDataRef targetData);
}
