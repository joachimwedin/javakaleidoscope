package org.jllvm;

import org.jllvm.pointers.LLVMValueRef;

public interface Analysis {
    void LLVMVerifyFunction(LLVMValueRef func, int i);

    int LLVMAbortProcessAction = 0;
    int LLVMPrintMessageAction = 1;
    int LLVMReturnStatusAction = 2;
}
