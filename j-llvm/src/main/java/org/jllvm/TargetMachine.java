package org.jllvm;

import org.jllvm.pointers.LLVMTargetDataRef;
import org.jllvm.pointers.LLVMTargetMachineRef;

public interface TargetMachine {
    String LLVMGetDefaultTargetTriple();
    LLVMTargetDataRef LLVMCreateTargetDataLayout(LLVMTargetMachineRef targetRef);
}
