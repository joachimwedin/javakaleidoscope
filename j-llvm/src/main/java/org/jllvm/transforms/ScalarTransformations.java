package org.jllvm.transforms;

import org.jllvm.pointers.LLVMPassManagerRef;

public interface ScalarTransformations {
    void LLVMAddInstructionCombiningPass(LLVMPassManagerRef passManager);
    void LLVMAddReassociatePass(LLVMPassManagerRef passManager);
    void LLVMAddGVNPass(LLVMPassManagerRef passManager);
    void LLVMAddCFGSimplificationPass(LLVMPassManagerRef passManager);
}
