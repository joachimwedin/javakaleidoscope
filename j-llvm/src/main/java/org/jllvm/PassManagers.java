package org.jllvm;

import org.jllvm.pointers.LLVMModuleRef;
import org.jllvm.pointers.LLVMPassManagerRef;
import org.jllvm.pointers.LLVMValueRef;

public interface PassManagers {
    void LLVMInitializeFunctionPassManager(LLVMPassManagerRef passManager);
    LLVMPassManagerRef LLVMCreateFunctionPassManagerForModule(LLVMModuleRef module);
    void LLVMRunFunctionPassManager(LLVMPassManagerRef functionPassManager, LLVMValueRef func);
}
