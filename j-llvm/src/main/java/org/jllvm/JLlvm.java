package org.jllvm;

import com.sun.jna.Library;
import com.sun.jna.Native;
import org.jllvm.pointers.*;
import org.jllvm.core.BasicBlock;
import org.jllvm.core.Contexts;
import org.jllvm.core.Modules;
import org.jllvm.core.types.FunctionTypes;
import org.jllvm.core.types.IntegerTypes;
import org.jllvm.core.types.SequentialTypes;
import org.jllvm.core.values.GeneralAPIs;
import org.jllvm.core.values.constants.CompositeConstants;
import org.jllvm.core.values.constants.FunctionParameters;
import org.jllvm.core.values.constants.ScalarConstants;
import org.jllvm.transforms.ScalarTransformations;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

public interface JLlvm extends Library, Analysis, BasicBlock, BitWriter, CompositeConstants, CustomJLlvmMethods, Contexts, FunctionParameters, FunctionTypes, GeneralAPIs, IntegerTypes, InstructionBuilders, Modules, PassManagers, ScalarConstants, ScalarTransformations, SequentialTypes, TargetInformation, TargetMachine {

    JLlvm INSTANCE = loadLlvmLibrary();

    /**
     * Loads the JLLVM shared library.
     * This will create a temporary directory, copy the libmyLlvmLibrary.so file from the resource folder to the temp dir and finally load the copy.
     * The reason for doing this is to be able to load the shared library when it is packaged inside of a jar file.
     * @return the LLVM library interface
     */
    static JLlvm loadLlvmLibrary() {
        try {
            String myLibraryName = "libmyLlvmLibrary.so";
            File myTemporaryLibraryDirectory = Files.createTempDirectory("myLlvmLibrary").toFile();
            myTemporaryLibraryDirectory.deleteOnExit();
            File myTemporaryLibraryFile = new File(myTemporaryLibraryDirectory, myLibraryName);
            myTemporaryLibraryFile.deleteOnExit();
            InputStream in = JLlvm.class.getResource("/" + myLibraryName).openStream();
            Files.copy(in, myTemporaryLibraryFile.toPath());
            System.load(myTemporaryLibraryFile.getAbsolutePath());
            return Native.loadLibrary(myTemporaryLibraryFile.getAbsolutePath(), JLlvm.class);
        } catch (IOException e) {
            e.printStackTrace();
            throw new UnsupportedOperationException("Failed to setup JLlvm.");
        }
    }
}

