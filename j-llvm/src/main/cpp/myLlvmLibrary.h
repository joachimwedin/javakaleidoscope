#ifndef MY_LLVM_WRAPPER
#define MY_LLVM_WRAPPER

extern "C" void initializeNative();

extern "C" void initializeAll();

extern "C" LLVMTargetMachineRef createDefaultTargetMachine();

extern "C" void emitObjectFile(LLVMTargetMachineRef targetMachine, LLVMModuleRef module, char* fileName);

extern "C" LLVMExecutionEngineRef createExecutionEngine(LLVMModuleRef mod);

extern "C" void runMainFunction(LLVMExecutionEngineRef engine);

extern "C" char* runBuildOutputFunction(LLVMExecutionEngineRef engine);

extern "C" double evaluateTopLevelExpression(LLVMExecutionEngineRef engine, char* topLevelExpressionFunctionName);

#endif