
#include <llvm-c/Core.h>
#include <llvm-c/ExecutionEngine.h>
#include <llvm-c/Target.h>
#include <llvm-c/Analysis.h>
#include <llvm-c/BitWriter.h>
#include <llvm-c/Analysis.h>
#include <llvm-c/TargetMachine.h>

#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>

#include "myLlvmLibrary.h"

/**
 * Convenience method that calls multiple LLVM initialize methods.
 */
extern "C" void initializeNative() {
    LLVMInitializeNativeTarget();
    LLVMInitializeNativeAsmPrinter();
    LLVMInitializeNativeAsmParser();
}

/**
 * Convenience method that calls multiple LLVM initialize methods.
 */
extern "C" void initializeAll() {
    LLVMInitializeAllTargetInfos();
    LLVMInitializeAllTargets();
    LLVMInitializeAllTargetMCs();
    LLVMInitializeAllAsmParsers();
    LLVMInitializeAllAsmPrinters();
}

/**
 * Finds the default target for the host machine.
 */
static LLVMTargetRef getDefaultTarget() {
    char* targetTriple = LLVMGetDefaultTargetTriple();
    LLVMTargetRef targetRef = NULL;
    char *error = NULL;
    LLVMGetTargetFromTriple(targetTriple, &targetRef, &error);

    if (!targetRef) {
        fprintf(stderr, "failed to get the the default target\n");
        abort();
    }

    return targetRef;
}

/**
 * Creates a new LLVM TargetMachine by using the default target for the host machine.
 *
 * @return the created LLVM TargetMachine
 */
extern "C" LLVMTargetMachineRef createDefaultTargetMachine() {
    LLVMTargetRef target = getDefaultTarget();
    char* triple = LLVMGetDefaultTargetTriple();
    std::string cpu = "generic";
    std::string features = "";
    LLVMCodeGenOptLevel level = LLVMCodeGenLevelDefault;
    LLVMRelocMode reloc = LLVMRelocDefault;
    LLVMCodeModel codeModel = LLVMCodeModelDefault;

    return LLVMCreateTargetMachine(target, triple, cpu.c_str(), features.c_str(), level, reloc, codeModel);
}

/**
 * Emits an object file.
 *
 * @param targetMachine the LLVM TargetMachine
 * @param module the LLVM module
 * @param fileName the name of the file that object code is emmited to
 */
extern "C" void emitObjectFile(LLVMTargetMachineRef targetMachine, LLVMModuleRef module, char* fileName) {
    char *error = NULL;
    if (LLVMTargetMachineEmitToFile(targetMachine, module, fileName, LLVMObjectFile, &error)) {
        fprintf(stderr, "failed to emit object file\n");
        abort();
    }
}

/**
 * Creates and returns an LLVM execution engine.
 * Calls abort() If the execution engine fails to be created.
 *
 * @param mod the LLVM module
 * @return the created execution engine
 */
extern "C" LLVMExecutionEngineRef createExecutionEngine(LLVMModuleRef mod) {
    LLVMExecutionEngineRef engine;
    char *error = NULL;
    if (LLVMCreateExecutionEngineForModule(&engine, mod, &error) != 0) {
        fprintf(stderr, "failed to create execution engine\n");
        abort();
    }
    return engine;
}

/**
 * Runs a function with no arguments and an int return value using a supplied LLVM execution engine.
 *
 * @param engine the LLVM execution engine
 * @param functionName the name of the function.
 */
extern "C" int runIntProducerFunction(LLVMExecutionEngineRef engine, char* functionName) {
    uint64_t functionAddress = LLVMGetFunctionAddress(engine, functionName);
    int (*FP)() = (int (*)()) functionAddress;
    return FP();
}

/**
 * Runs a function with no arguments and a string return value using a supplied LLVM execution engine.
 *
 * @param engine the LLVM execution engine
 * @param functionName the name of the function.
 * @return the string obtained by calling the function.
 */
extern "C" char* runStringProducerFunction(LLVMExecutionEngineRef engine, char* functionName) {
    uint64_t functionAddress = LLVMGetFunctionAddress(engine, functionName);
    char* (*FP)() = (char* (*)()) functionAddress;
    return FP();
}

/**
 * Evaluates an anonymous function associated with a top level expression and returns the result
 *
 * @param engine the LLVM execution engine
 * @param topLevelExpressionFunctionName the name of the anonymous function associated with the top level expression
 * @return the resulting double from evaluaing the top level expression
 */
extern "C" double evaluateTopLevelExpression(LLVMExecutionEngineRef engine, char* topLevelExpressionFunctionName) {
    uint64_t functionAddress = LLVMGetFunctionAddress(engine, topLevelExpressionFunctionName);
    double (*FP)() = (double (*)()) functionAddress;
    return FP();
}
