package org.javakaleidoscope.compiler;

import org.jllvm.JLlvm;
import org.jllvm.pointers.LLVMExecutionEngineRef;
import org.jllvm.pointers.LLVMModuleRef;
import org.jllvm.pointers.LLVMTargetDataRef;
import org.jllvm.pointers.LLVMTargetMachineRef;

import java.util.StringJoiner;

public class KaleidoscopeBackEnd {

    static JLlvm LLVM = KaleidoscopeIR.LLVM;

    /**
     * Generates object files for an IR.
     * The name of the object files are the same as the LLVM module identifiers followed by a .o file ending.
     * @param ir a JavaKaleidoscope IR to generate object files for.
     */
    public void genObjectCode(KaleidoscopeIR ir) {
        LLVM.initializeAll();

        for (LLVMModuleRef module: ir.getModules()) {
            setDefaultDataLayoutAndTarget(module);
            genObjectCode(module);
        }
    }

    /**
     * Convenience method that sets the data layout and the target of an LLVM module to their default values.
     * @param module the LLVM module.
     */
    public void setDefaultDataLayoutAndTarget(LLVMModuleRef module) {
        LLVMTargetMachineRef targetMachine = LLVM.createDefaultTargetMachine();
        LLVMTargetDataRef dataLayout = LLVM.LLVMCreateTargetDataLayout(targetMachine);
        String dataLayoutString = LLVM.LLVMCopyStringRepOfTargetData(dataLayout);
        String targetTriple = LLVM.LLVMGetDefaultTargetTriple();

        LLVM.LLVMSetDataLayout(module, dataLayoutString);
        LLVM.LLVMSetTarget(module, targetTriple);
    }

    /**
     * Generates object code for a LLVM module using the default target for the host machine.
     * The name of the created object file is the same as the module identifier followed by a .o file ending.
     * @param module the LLVM module.
     */
    public void genObjectCode(LLVMModuleRef module) {
        LLVMTargetMachineRef targetMachine = LLVM.createDefaultTargetMachine();
        LLVM.emitObjectFile(targetMachine, module, LLVM.LLVMGetModuleIdentifier(module, new int[1]) + ".o");
    }

    /**
     * Executes the top level expressions of a JavaKaleidoscope IR and builds a string with the result.
     *
     * @param ir the JavaKaleidoscope IR to execute.
     * @return a string containing the result of evaluating the top level expressions of the IR.
     */
    public String executeWithJITEngine(KaleidoscopeIR ir) {
        LLVM.initializeNative();
        StringJoiner sj = new StringJoiner("\n");
        for (LLVMModuleRef module : ir.getModules()) {
            LLVMExecutionEngineRef engine = LLVM.createExecutionEngine(module);
            String res = LLVM.runStringProducerFunction(engine, "buildOutput");
            sj.add(res);
        }
        return sj.toString();
    }
}
